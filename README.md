# PLUGIN documentatie

## Scope en doelgroep

Deze repository bevat de broncode voor de documentatie van het PLUGIN project. Het is gemaakt in [Quarto](https://quarto.org). Op dit moment is het document nog niet gepubliceerd. De intentie is dat het tzt wel zal gebeuren.


> De website wordt gepubliceerd op GitLab pages: https://dkapitan.gitlab.io/plugin-documentatie/

## How to work with Quarto and GitLab

### Getting started

- Read the short introduction on how author documents in markdown using Quarto ([link](https://quarto.org/docs/authoring/markdown-basics.html))
- Read the short introduction on how to use branches in the GitHub flow for version control ([link](https://docs.github.com/en/get-started/quickstart/github-flow)).
  - The short introduction to this workflow is that everyone works in their own branch, and changes to the document are merged into the `main` branch.
